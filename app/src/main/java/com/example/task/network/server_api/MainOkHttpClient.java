package com.example.task.network.server_api;


import android.support.annotation.NonNull;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

public class MainOkHttpClient extends OkHttpClient {

    public static final int TYPE_SAFE = 1;
    public static final int TYPE_UNSAFE = 2;
    public static final String SSL = "SSL";

    private static int TIMEOUT_SEC = 15;

    private OkHttpClient mOkHttpClient;

    public MainOkHttpClient(int type) {

        super();
        if (type == TYPE_SAFE) {
            mOkHttpClient = getSafeOkHttpClient();
        } else if (type == TYPE_UNSAFE) {
            mOkHttpClient = getUnsafeOkHttpClient();
        }

    }

    @NonNull
    private OkHttpClient getSafeOkHttpClient() {
        Builder builder = new Builder();
        builder.connectTimeout(TIMEOUT_SEC, TimeUnit.SECONDS);
        builder.readTimeout(TIMEOUT_SEC, TimeUnit.SECONDS);
        builder.addInterceptor(getHttpLoggingInterceptor());

        return builder.build();
    }

    private OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            X509TrustManager x509TrustManager = new X509TrustManager() {
                @Override
                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                @Override
                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[]{};
                }
            };
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    x509TrustManager
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance(SSL);
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            Builder builder = new Builder();
            builder.sslSocketFactory(sslSocketFactory, x509TrustManager);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
            builder.connectTimeout(TIMEOUT_SEC, TimeUnit.SECONDS);
            builder.readTimeout(TIMEOUT_SEC, TimeUnit.SECONDS);
            builder.addInterceptor(getHttpLoggingInterceptor());

            return builder.build();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }


    }

    @NonNull
    private HttpLoggingInterceptor getHttpLoggingInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return interceptor;
    }

    public OkHttpClient getOkHttpClient() {
        return mOkHttpClient;
    }

}
