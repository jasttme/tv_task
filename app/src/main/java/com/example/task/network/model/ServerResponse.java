package com.example.task.network.model;

import com.google.gson.annotations.Expose;

import java.util.List;

public class ServerResponse {
    @Expose
    private List<ProgramsModel> items;

    @Expose
    int items_number;
    @Expose
    int total;
    @Expose
    int offset;
    @Expose
    int hasMore;

    public ServerResponse() {
    }

    public List<ProgramsModel> getItems() {
        return items;
    }

    public void setItems(List<ProgramsModel> items) {
        this.items = items;
    }

    public int getItems_number() {
        return items_number;
    }

    public void setItems_number(int items_number) {
        this.items_number = items_number;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getHasMore() {
        return hasMore;
    }

    public void setHasMore(int hasMore) {
        this.hasMore = hasMore;
    }
}
