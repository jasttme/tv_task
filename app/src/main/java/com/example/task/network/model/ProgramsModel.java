package com.example.task.network.model;

import com.google.gson.annotations.Expose;

public class ProgramsModel {
    @Expose
    private int id;
    @Expose
    private String icon;
    @Expose
    private String name;

    public ProgramsModel() {
    }

    public ProgramsModel(int id, String icon, String name) {
        this.id = id;
        this.icon = icon;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
