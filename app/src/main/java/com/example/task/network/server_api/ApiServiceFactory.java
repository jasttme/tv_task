package com.example.task.network.server_api;

import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;



public class ApiServiceFactory {

    public static final String BASE_URL = "http://oll.tv";

    private ApiServiceFactory() {

    }

    public static <T> T createRetrofitService(final Class<T> clazz) {
        Retrofit builder = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()))
                .client(new MainOkHttpClient(MainOkHttpClient.TYPE_UNSAFE).getOkHttpClient())
                .build();

        return builder.create(clazz);
    }

}
