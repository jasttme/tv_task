package com.example.task.network.server_api;


import com.example.task.network.model.ServerResponse;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;
import rx.schedulers.Schedulers;

public class ServerService implements ServerApi<Observable> {

    private ServerRxApi api;

    public ServerService() {
        api = ApiServiceFactory.createRetrofitService(ServerRxApi.class);
    }


    @Override
    public Observable<ServerResponse> loadPrograms(String serialNumber,
                                                   int borderId,
                                                   int direction) {
        return api.loadPrograms(
                serialNumber,
                borderId,
                direction
        ).subscribeOn(Schedulers.io());
    }

    private interface ServerRxApi {

        @GET("/demo")
        Observable<ServerResponse> loadPrograms(@Query("serial_number") String serial_number,
                                                @Query("borderId") int borderId,
                                                @Query("direction") int direction);


    }
}