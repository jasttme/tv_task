package com.example.task.network.server_api;


public interface ServerApi<T> {

    T loadPrograms(String serialNumber, int borderId, int direction);

}
