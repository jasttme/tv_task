package com.example.task.main.interactor;


import com.example.task.network.model.ServerResponse;

public interface FindItemsInteractor {

    rx.Observable<ServerResponse> loadPrograms(String serialNumber, int borderId, int direction);

}
