package com.example.task.main.presenter;

import com.example.task.main.interactor.FindItemsInteractor;
import com.example.task.main.model.ProgramsViewModelMapper;
import com.example.task.main.view.MainView;
import com.example.task.network.model.ProgramsModel;
import com.example.task.network.model.ServerResponse;
import com.example.task.utils.DeviceUtils;

import java.util.List;

import rx.Subscriber;

public class MainPresenterImpl implements MainPresenter {

    public static final int LOAD_UP = -1;
    public static final int LOAD = 0;
    public static final int LOAD_DOWN = 1;
    private final ProgramsViewModelMapper programsViewModelMapper;

    private MainView mainView;
    private FindItemsInteractor findItemsInteractor;
    private String uuid;

    public MainPresenterImpl(MainView mainView,
                             FindItemsInteractor findItemsInteractor,
                             ProgramsViewModelMapper programsViewModelMapper) {
        this.mainView = mainView;
        this.findItemsInteractor = findItemsInteractor;
        this.programsViewModelMapper = programsViewModelMapper;
    }

    @Override
    public void loadPrograms(final int direction, int lasItemId) {
        if (uuid == null || uuid.equals("")) {
            uuid = DeviceUtils.getDeviceUUID(mainView.getContext());
        }

        findItemsInteractor.loadPrograms(uuid, lasItemId, direction).subscribe(new Subscriber<ServerResponse>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onNext(ServerResponse serverResponse) {
                List<ProgramsModel> items = serverResponse.getItems();
                mainView.setItems(programsViewModelMapper.map(items), direction);
                mainView.setSRLParams(serverResponse.getOffset());
            }
        });

    }

}
