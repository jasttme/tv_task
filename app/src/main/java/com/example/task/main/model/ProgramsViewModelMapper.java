package com.example.task.main.model;

import com.example.task.network.model.ProgramsModel;

import java.util.ArrayList;
import java.util.List;

public class ProgramsViewModelMapper {

    public ProgramsViewModelMapper() {
    }

    public ProgramsViewModel map(ProgramsModel model){
        ProgramsViewModel programsViewModel = new ProgramsViewModel();
        programsViewModel.setId(model.getId());
        programsViewModel.setIcon(model.getIcon());
        programsViewModel.setName(model.getName());
        return programsViewModel;
    }

    public List<ProgramsViewModel> map(List<ProgramsModel> models){
        List<ProgramsViewModel> mappedList = new ArrayList<>();
        for (ProgramsModel model: models) {
            ProgramsViewModel programsViewModel = new ProgramsViewModel();
            programsViewModel.setId(model.getId());
            programsViewModel.setIcon(model.getIcon());
            programsViewModel.setName(model.getName());
            mappedList.add(programsViewModel);
        }
        return mappedList;
    }

}
