package com.example.task.main.presenter;

public interface MainPresenter {

    void loadPrograms(int direction, int borderId);
}
