
package com.example.task.main.interactor;

import com.example.task.network.model.ServerResponse;
import com.example.task.network.server_api.ServerService;

import rx.Observable;
import rx.Subscriber;

public class FindItemsInteractorImpl implements FindItemsInteractor {

    ServerService serverService;
    private Subscriber<? super ServerResponse> mSubscriber;

    public FindItemsInteractorImpl(ServerService serverService) {
        this.serverService = serverService;
    }

    @Override
    public Observable<ServerResponse> loadPrograms(final String serialNumber, final int borderId, final int direction) {
        return Observable.create(new Observable.OnSubscribe<ServerResponse>() {
            @Override
            public void call(Subscriber<? super ServerResponse> subscriber) {
                mSubscriber = subscriber;
                serverService.loadPrograms(serialNumber, borderId, direction)
                        .subscribe(new Subscriber<ServerResponse>() {
                            @Override
                            public void onCompleted() {
                            }

                            @Override
                            public void onError(Throwable e) {
                                e.printStackTrace();
                            }

                            @Override
                            public void onNext(ServerResponse serverResponse) {
                                mSubscriber.onNext(serverResponse);
                            }
                        });
            }
        });
    }
}
