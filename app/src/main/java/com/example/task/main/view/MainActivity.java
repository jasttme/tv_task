
package com.example.task.main.view;

import android.Manifest;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.task.R;
import com.example.task.main.interactor.FindItemsInteractorImpl;
import com.example.task.main.model.ProgramsViewModel;
import com.example.task.main.model.ProgramsViewModelMapper;
import com.example.task.main.presenter.MainPresenter;
import com.example.task.main.presenter.MainPresenterImpl;
import com.example.task.network.server_api.ServerService;
import com.example.task.utils.PermissionsUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.task.utils.PermissionsUtils.isPermissionDenied;

public class MainActivity extends AppCompatActivity implements MainView {

    public static final int PERMISSION_REQUEST_CODE = 777;
    public static final int LOAD_OFFSET = 5;

    @BindView(R.id.root_view)
    protected RelativeLayout rootView;

    @BindView(R.id.rv_channels_list)
    protected RecyclerView rvChannelsList;

    @BindView(R.id.srlChannels)
    protected SwipeRefreshLayout srlChannels;

    private MainPresenter presenter;
    private ChannelsListAdapter channelsListAdapter;
    private LinearLayoutManager lmChannelsList;
    private RecyclerView.OnScrollListener onScrollListener;
    private boolean isLoading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initList();
        requestPermissions();
        initPresenter();
    }

    private void initPresenter() {
        ServerService serverService = new ServerService();
        FindItemsInteractorImpl findItemsInteractor = new FindItemsInteractorImpl(serverService);
        ProgramsViewModelMapper programsViewModelMapper = new ProgramsViewModelMapper();
        presenter = new MainPresenterImpl(this, findItemsInteractor, programsViewModelMapper);
    }

    private void initList() {
        channelsListAdapter = new ChannelsListAdapter(MainActivity.this);
        channelsListAdapter.setOnItemClickListener(new ChannelsListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(ProgramsViewModel channelItem, int position) {
                showMessage(channelItem.getName());
            }
        });
        rvChannelsList.setAdapter(channelsListAdapter);
        lmChannelsList = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false);
        rvChannelsList.setLayoutManager(lmChannelsList);

        onScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int totalItemCount = lmChannelsList.getItemCount();
                int firstVisibleItemPosition = lmChannelsList.findFirstVisibleItemPosition();
                int lastVisibleItemPosition = lmChannelsList.findLastVisibleItemPosition();

                if (!isLoading) {
                    if (firstVisibleItemPosition < LOAD_OFFSET) {
                        isLoading = true;
                        presenter.loadPrograms(MainPresenterImpl.LOAD_UP, channelsListAdapter.getFirstItemId());
                        showLoading();
                    } else {
                        if (totalItemCount - lastVisibleItemPosition < LOAD_OFFSET) {
                            isLoading = true;
                            presenter.loadPrograms(MainPresenterImpl.LOAD_DOWN, channelsListAdapter.getLasItemId());
                            showLoading();
                        }
                    }
                }
            }
        };
        rvChannelsList.addOnScrollListener(
                onScrollListener);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvChannelsList.getContext(),
                lmChannelsList.getOrientation());
        rvChannelsList.addItemDecoration(dividerItemDecoration);

        srlChannels.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                requestPermissions();
            }
        });
        rvChannelsList.addOnScrollListener((new SwipeRefreshLayoutToggleScrollListener(srlChannels)));
    }


    @Override
    protected void onDestroy() {
        rvChannelsList.removeOnScrollListener(
                onScrollListener);
        super.onDestroy();
    }

    @Override
    public void showLoading() {
        srlChannels.setRefreshing(true);
    }

    @Override
    public void dismissLoading() {
        srlChannels.setRefreshing(false);
    }

    @Override
    public void setItems(final List<ProgramsViewModel> items, final int direction) {
        rvChannelsList.post(new Runnable() {
            @Override
            public void run() {
                isLoading = false;
                dismissLoading();
                channelsListAdapter.setItems(items, direction);
                if (direction == 0) {
                    rvChannelsList.scrollToPosition(10);
                }
            }
        });

    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public Context getContext() {
        return MainActivity.this;
    }

    @Override
    public void setSRLParams(int offset) {
        if (offset == 0){
            srlChannels.setEnabled(true);
        } else {
            srlChannels.setEnabled(false);
        }
    }

    private void requestPermissions() {
        if (PermissionsUtils.isPermissionGranted(MainActivity.this,
                Manifest.permission.CAMERA)) {
            showLoading();
            presenter.loadPrograms(0, channelsListAdapter.getLasItemId());
        } else {
            PermissionsUtils.requestPermission(MainActivity.this, rootView,
                    getString(R.string.permission_rationale_read_phone), new String[]{Manifest.permission.READ_PHONE_STATE},
                    PERMISSION_REQUEST_CODE);
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE: {
                if (isPermissionDenied(grantResults)) {
                    dismissLoading();
                    PermissionsUtils.checkIfPermissionsForeverDenied(MainActivity.this, rootView, permissions, grantResults);
                } else {
                    showLoading();
                    presenter.loadPrograms(0, channelsListAdapter.getLasItemId());
                }

            }
        }
    }


}
