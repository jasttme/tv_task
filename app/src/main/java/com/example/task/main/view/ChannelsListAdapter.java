package com.example.task.main.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.task.R;
import com.example.task.main.model.ProgramsViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.task.main.presenter.MainPresenterImpl.LOAD;
import static com.example.task.main.presenter.MainPresenterImpl.LOAD_DOWN;
import static com.example.task.main.presenter.MainPresenterImpl.LOAD_UP;

class ChannelsListAdapter extends RecyclerView.Adapter<ChannelsListAdapter.ChannelsListViewHolder> {

    protected Context mContext;
    protected ArrayList<ProgramsViewModel> mItems;
    private OnItemClickListener onItemClickListener;

    public ChannelsListAdapter(Context mContext) {
        this.mContext = mContext;
        mItems = new ArrayList<>();
    }

    @Override
    public ChannelsListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_channel_list, null, false);

        return new ChannelsListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ChannelsListViewHolder holder, final int position) {
        final ProgramsViewModel programsViewModel = getItem(position);

        holder.getRootView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(programsViewModel, position);
                }
            }
        });

        holder.getTvChannelName().setText(programsViewModel.getName());

        ImageView ivChannelLogo = holder.getIvChannelLogo();
        Glide.with(mContext)
                .load(programsViewModel.getIcon())
                .asBitmap()
                .fitCenter()
                .placeholder(R.mipmap.ic_launcher)
                .into(ivChannelLogo);

    }

    private ProgramsViewModel getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void setItems(List<ProgramsViewModel> orderItems, int direction) {
        switch (direction) {
            case LOAD:
                mItems.clear();
                mItems.addAll(orderItems);
                notifyDataSetChanged();
                break;
            case LOAD_UP:
                mItems.addAll(0, orderItems);
                notifyItemRangeInserted(0, orderItems.size());
                break;
            case LOAD_DOWN:
                mItems.addAll(mItems.size(), orderItems);
                notifyItemRangeInserted(mItems.size(), orderItems.size());
                break;
        }
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public int getFirstItemId() {
        if (mItems.isEmpty()) {
            return 0;
        } else {
            return mItems.get(0).getId();
        }
    }

    public int getLasItemId() {
        if (mItems.isEmpty()) {
            return 0;
        } else {
            return mItems.get(mItems.size() - 1).getId();
        }
    }

    public interface OnItemClickListener {
        void onItemClick(ProgramsViewModel ProgramsViewModel, int position);
    }

    static class ChannelsListViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.root_view)
        protected LinearLayout rootView;

        @BindView(R.id.tv_channel_name)
        protected TextView tvChannelName;

        @BindView(R.id.iv_channel_logo)
        protected ImageView ivChannelLogo;


        public ChannelsListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public LinearLayout getRootView() {
            return rootView;
        }

        public TextView getTvChannelName() {
            return tvChannelName;
        }

        public ImageView getIvChannelLogo() {
            return ivChannelLogo;
        }
    }


}
