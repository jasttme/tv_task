package com.example.task.main.view;

import android.content.Context;

import com.example.task.main.model.ProgramsViewModel;

import java.util.List;

public interface MainView {

    void showLoading();

    void dismissLoading();

    void showMessage(String message);

    void setItems(List<ProgramsViewModel> channelItems, int direction);

    Context getContext();

    void setSRLParams(int offset);
}
