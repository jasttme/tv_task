package com.example.task.utils;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.task.R;


public class PermissionsUtils {

    public static final String SECURITY_EXCEPTION_MESSAGE = "All parameters should be field and not null";
    public static final String PACKAGE = "package";

    private PermissionsUtils() {

    }

    public static boolean isPermissionGranted(AppCompatActivity activity, String requestPermission) {

        if (activity == null ||
                requestPermission == null ||
                requestPermission.equals("")) {
            makeSecurityExeption(SECURITY_EXCEPTION_MESSAGE);
        }

        int permission = ActivityCompat.checkSelfPermission(activity, requestPermission);
        if (permission != PackageManager.PERMISSION_GRANTED) {
            return false;
        } else {
            return true;
        }

    }

    public static void requestPermission(final Fragment fragment, View layout, String permText, String requestPermission, final int requestCode) {
        final String[] permisions = {requestPermission};
        if (fragment.shouldShowRequestPermissionRationale(requestPermission)) {
            if (layout != null) {

                Snackbar.make(layout, permText,
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.ok, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                fragment.requestPermissions(permisions, requestCode);
                            }
                        })
                        .show();
            }

        } else {
            fragment.requestPermissions(permisions, requestCode);
        }
    }

        public static void requestPermission(final AppCompatActivity activity, View layout, String permText, final String[] permissions, final int requestCode) {
            boolean needShowRequestPermissionRationale = false;
            for (String permission : permissions
                    ) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                    needShowRequestPermissionRationale = true;
                } else if (!ActivityCompat.shouldShowRequestPermissionRationale(activity, permission) &&
                        ActivityCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
                    needShowRequestPermissionRationale = false;
                    break;
                }
            }
            if (needShowRequestPermissionRationale) {
                Snackbar.make(layout, permText,
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.ok, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                ActivityCompat.requestPermissions(activity, permissions, requestCode);
                            }
                        })
                        .show();
            } else {
                ActivityCompat.requestPermissions(activity, permissions, requestCode);
            }
        }




    private static void makeSecurityExeption(String s) {
        throw new SecurityException(s);
    }

    public static void checkIfPermissionsForeverDenied(final Activity activity, View layout, String[] permissions, int[] grantResults) {
        boolean isForeverDenied = false;
        for (int i = 0; i < permissions.length; i++) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(activity, permissions[i]) &&
                    grantResults[i] == PackageManager.PERMISSION_DENIED) {
                isForeverDenied = true;
                break;
            }
        }

        if (isForeverDenied) {
            Snackbar.make(layout, R.string.permissions_not_granted,
                    Snackbar.LENGTH_LONG)
                    .setAction(R.string.settings, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent();
                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts(PACKAGE, activity.getPackageName(), null);
                            intent.setData(uri);
                            activity.startActivity(intent);
                        }
                    })
                    .show();
        } else {
            Snackbar.make(layout, R.string.permissions_not_granted,
                    Snackbar.LENGTH_SHORT).show();
        }
    }

    public static boolean isPermissionDenied(@NonNull int[] grantResults) {
        for (Integer permissionResult : grantResults) {
            if (permissionResult != PackageManager.PERMISSION_GRANTED) {
                return true;
            }
        }
        return false;
    }


}
