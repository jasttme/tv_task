package com.example.task.utils;

import android.content.Context;
import android.telephony.TelephonyManager;

public class DeviceUtils {

    public static String getDeviceUUID(Context context){
        TelephonyManager tManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return tManager.getDeviceId();
    }

}
